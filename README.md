# Adfab technical test

Simple API making you save, update, list and delete categories and products.

## Request with Postman

When running locally you can list all your products with a simple get on `localhost:9000/api/products`
ou can sort them by prices with : `localhost:9000/api/products?sort=-price` or `localhost:9000/api/products?sort=price`

To create or update you should use `x-www-form-urlencode`

## Commands

After you generate your project, these commands are available in `package.json`.

```bash
npm test # test using Jest
npm run coverage # test and open the coverage report in the browser
npm run lint # lint using ESLint
npm run dev # run the API in development mode
npm run prod # run the API in production mode
npm run docs # generate API docs
```

## Playing locally

First, you will need to install and run [MongoDB](https://www.mongodb.com/) in another terminal instance.

```bash
$ mongod
```

Then, run the server in development mode.

```bash
$ npm run dev
Express server listening on http://0.0.0.0:9000, in development mode
```

If you choose to generate the authentication API, you can start to play with it.
> Note that creating and authenticating users needs a master key (which is defined in the `.env` file)

