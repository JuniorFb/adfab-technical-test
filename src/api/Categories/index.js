import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
export Categories, { schema } from './model'

const router = new Router()
const { name, description } = schema.tree

/**
 * @api {post} /api/categories Create categories
 * @apiName CreateCategories
 * @apiGroup Categories
 * @apiParam name Categories's name.
 * @apiParam description Categories's description.
 * @apiSuccess {Object} categories Categories's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Categories not found.
 */
router.post('/',
  body({ name, description }),
  create)

/**
 * @api {get} /api/categories Retrieve categories
 * @apiName RetrieveCategories
 * @apiGroup Categories
 * @apiUse listParams
 * @apiSuccess {Object[]} categories List of categories.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/',
  query(),
  index)

/**
 * @api {get} /api/categories/:id Retrieve categories
 * @apiName RetrieveCategories
 * @apiGroup Categories
 * @apiSuccess {Object} categories Categories's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Categories not found.
 */
router.get('/:id',
  show)

/**
 * @api {put} /api/categories/:id Update categories
 * @apiName UpdateCategories
 * @apiGroup Categories
 * @apiParam name Categories's name.
 * @apiParam description Categories's description.
 * @apiSuccess {Object} categories Categories's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Categories not found.
 */
router.put('/:id',
  body({ name, description }),
  update)

/**
 * @api {delete} /api/categories/:id Delete categories
 * @apiName DeleteCategories
 * @apiGroup Categories
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Categories not found.
 */
router.delete('/:id',
  destroy)

export default router
