import request from 'supertest'
import { apiRoot } from '../../config'
import express from '../../services/express'
import routes, { Categories } from '.'

const app = () => express(apiRoot, routes)

let categories

beforeEach(async () => {
  categories = await Categories.create({})
})

test('POST /api/categories 201', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ name: 'test', description: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.name).toEqual('test')
  expect(body.description).toEqual('test')
})

test('GET /api/categories 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(200)
  expect(Array.isArray(body)).toBe(true)
})

test('GET /api/categories/:id 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${categories.id}`)
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(categories.id)
})

test('GET /api/categories/:id 404', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})

test('PUT /api/categories/:id 200', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${categories.id}`)
    .send({ name: 'test', description: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(categories.id)
  expect(body.name).toEqual('test')
  expect(body.description).toEqual('test')
})

test('PUT /api/categories/:id 404', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ name: 'test', description: 'test' })
  expect(status).toBe(404)
})

test('DELETE /api/categories/:id 204', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${categories.id}`)
  expect(status).toBe(204)
})

test('DELETE /api/categories/:id 404', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})
