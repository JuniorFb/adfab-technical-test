import { Products } from '.'

let products

beforeEach(async () => {
  products = await Products.create({ price: 'test', description: 'test', barCode: 'test', name: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = products.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(products.id)
    expect(view.price).toBe(products.price)
    expect(view.description).toBe(products.description)
    expect(view.barCode).toBe(products.barCode)
    expect(view.name).toBe(products.name)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = products.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(products.id)
    expect(view.price).toBe(products.price)
    expect(view.description).toBe(products.description)
    expect(view.barCode).toBe(products.barCode)
    expect(view.name).toBe(products.name)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
