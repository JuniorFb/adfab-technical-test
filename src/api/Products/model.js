import mongoose, { Schema } from 'mongoose'

const productsSchema = new Schema({
  price: {
    type: Number,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  barCode: {
    type: Number,
    required: true
  },
  name: {
    type: String,
    required: true
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

productsSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      price: this.price,
      description: this.description,
      barCode: this.barCode,
      name: this.name,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Products', productsSchema)

export const schema = model.schema
export default model
