import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
export Products, { schema } from './model'

const router = new Router()
const { price, description, barCode, name } = schema.tree

/**
 * @api {post} /api/products Create products
 * @apiName CreateProducts
 * @apiGroup Products
 * @apiParam price Products's price.
 * @apiParam description Products's description.
 * @apiParam barCode Products's barCode.
 * @apiParam name Products's name.
 * @apiSuccess {Object} products Products's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Products not found.
 */
router.post('/',
  body({ price, description, barCode, name }),
  create)

/**
 * @api {get} /api/products Retrieve products
 * @apiName RetrieveProducts
 * @apiGroup Products
 * @apiUse listParams
 * @apiSuccess {Object[]} products List of products.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/',
  query(),
  index)

/**
 * @api {get} /api/products/:id Retrieve products
 * @apiName RetrieveProducts
 * @apiGroup Products
 * @apiSuccess {Object} products Products's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Products not found.
 */
router.get('/:id',
  show)

/**
 * @api {put} /api/products/:id Update products
 * @apiName UpdateProducts
 * @apiGroup Products
 * @apiParam price Products's price.
 * @apiParam description Products's description.
 * @apiParam barCode Products's barCode.
 * @apiParam name Products's name.
 * @apiSuccess {Object} products Products's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Products not found.
 */
router.put('/:id',
  body({ price, description, barCode, name }),
  update)

/**
 * @api {delete} /api/products/:id Delete products
 * @apiName DeleteProducts
 * @apiGroup Products
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Products not found.
 */
router.delete('/:id',
  destroy)

export default router
